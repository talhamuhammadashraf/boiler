import React, { Component } from 'react';
import image from '../assets/images/search.png';

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    console.log(this.state, 'this is');
    return (
      <div>
        <p>
          <input
            name="search"
            placeholder="What mattress are you looking for?"
            onChange={(event) => { this.setState({ [event.target.name]: event.target.value }); }}
          />
          <button><img src={image} alt="search" /></button>
        </p>
      </div>
    );
  }
}

export default App;
